<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Account Details</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/CDetails.css">

<style>
header{
display:flex;
justify-content:flex-end;
align-items:right;
padding:30px 90px;
}
button{
padding:9px 25px;
cursor:pointer;
border:none;
}
th, td {
  border-bottom: 1px solid #ddd;
}
tr:nth-child(even) {background-color: #f2f2f2;}
th {
  background-color: #185bf5;
  color: white;
}

</style>
</head>

<body>
 <Header>
 <a href="CAmount.jsp">
<button style="background-color: #185bf5;padding-left:15px; margin-left:100px; color: white; border-radius: 8px; height: 50px; width: 120px; font-size: 18px; border: none;">PAmount</button>
</a>
<a href="CShipment.jsp">
<button style="background-color: #185bf5;padding-left:15px; margin-left:100px; color: white; border-radius: 8px; height: 50px; width: 120px; font-size: 18px; border: none;">SDetails</button>
</a>
</Header>
	
    <div class="CDetails-form">
        <form action ="BDetails" method="post">
            <div class="form-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
            
            <div class="form-group">
              <input type="text" name="cname" class="form-control item" id="cname" placeholder="CustomerName" required="required">
            </div>
            <br>
            
            <div class="form-group">
            <input type="text" name="phoneno" class="form-control item" id="phoneno" placeholder="PhoneNo" required="required">
            </div>
            <br>
            
             
            <div class="form-group">
            <input type="number" name="pweight" class="form-control item" id="pweight" placeholder="Packet weight" required="required">
            </div>
            <br>
            
             <div class="form-group">Delivery Area : 
               <input type = "radio" name = "city" value="to Delhi" id="BtoD" required="required"> to Delhi  &nbsp;&nbsp;
              <input type = "radio" name = "city" value="to Hyderabad" id="BtoH" required="required"> to Hyderabad &nbsp;&nbsp;
            </div>
            <br>
            
            
            <div class="form-group">
                <button type="submit" class="btn btn-block submit">Submit</button>
            </div>
        </form>
        
         <div class="social-media">
            <h5></h5>
            <div class="social-icons">
                
            </div>
        </div>
    </div>
    </div>
    <div>
    <table align='center' border='0' style='width:20%;height:5px; move-down:30px;  border-collapse:collapse; font-size:100%;'>
    	 <thead>
     		 <tr>
        		 <th> Minimum Delivery Charge</th>
   			</tr>
    		<tr>
    			<th>
    			 Destination
    			</th>
    			<th>Amount in Rs</th>
    		 </tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>Hyderabad</td>
    			<td>500</td>
    		</tr>
    		<tr>
    			<td>Delhi</td>
    			<td>700</td>
    	    </tr>
    	</tbody>
    </table>
    
    
</body>
</html>
